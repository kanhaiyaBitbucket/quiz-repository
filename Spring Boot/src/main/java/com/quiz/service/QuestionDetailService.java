package com.quiz.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quiz.entity.QuestionDetail;
import com.quiz.repository.QuestionDetailRepository;

@Service
public class QuestionDetailService {

	@Autowired
	private QuestionDetailRepository questionDetailRepository;
	
	public Optional<QuestionDetail> getQuestionById(Integer id) {
		return questionDetailRepository.findById(id);
	}
	
	public List<QuestionDetail> getQuestionByCourseId(int id) {
		return questionDetailRepository.findByCourseId(id);
	}
}

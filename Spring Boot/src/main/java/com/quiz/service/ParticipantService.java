package com.quiz.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quiz.entity.Participant;
import com.quiz.model.ParticipantModel;
import com.quiz.model.Result1;
import com.quiz.repository.ParticipantRepository;

@Service
public class ParticipantService {

	@Autowired
	private ParticipantRepository participantRepository;
	
	public Optional<Participant> getParticipentById(Integer id) {
		return participantRepository.findById(id);
	}
	
	public void saveParticipent(ParticipantModel participant) {
		 participantRepository.saveParticipant(participant.getScore(), participant.getTimespent(), participant.getCourseId(), participant.getUserId());
	}
	
	public Participant getParticipent(int userId, int courseId) {
		return participantRepository.getParticipant(userId, courseId);
	}
	
	public Result1 getResult(int userId, int courseId) {
		return participantRepository.getResult(userId, courseId);
	}
	
	
}

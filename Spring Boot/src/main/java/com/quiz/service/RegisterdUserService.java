package com.quiz.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.quiz.entity.RegistredUser;
import com.quiz.model.User;
import com.quiz.repository.UserRepository;

@Service
public class RegisterdUserService {

	@Autowired
	private UserRepository userRepository;

	public Optional<RegistredUser> getUserById(Integer id) {
		return userRepository.findById(id);
	}

	public void saveUserDetail(RegistredUser registredUser) {
		userRepository.save(registredUser);
	}

	public User getUser(String email) {
		return userRepository.getUser(email);
	}
}

package com.quiz.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quiz.entity.CourseType;
import com.quiz.model.CourseDetail;
import com.quiz.repository.CourseTypeRepository;

@Service
public class CourseTypeService {

	@Autowired
	private CourseTypeRepository courseTypeRepository;

	public Optional<CourseType> getCourseTypeById(Integer id) {
		return courseTypeRepository.findById(id);
	}

	public List<CourseDetail> getCourses() {
		return courseTypeRepository.getCourseDetails();
	}

}

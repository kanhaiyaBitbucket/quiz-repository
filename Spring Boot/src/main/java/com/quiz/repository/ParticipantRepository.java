package com.quiz.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.quiz.entity.Participant;
import com.quiz.model.Result1;

public interface ParticipantRepository extends JpaRepository<Participant, Integer> {

	@Modifying
	@Query(value = "insert into participant  (score,timespent,course_id,user_id) values (?1,?2,?3,?4)", nativeQuery = true)
	@Transactional
	public void saveParticipant(int score,int timespent,int courseId,int userId);
	
	@Query(value = "select id,score,timespent from participant where user_id = ?1 and course_id = ?2", nativeQuery = true)
	public Participant getParticipant(int userId,int courseId);
	
	@Query(value = "select name,score,timespent from participant p join registred_user r on r.id=p.user_id where user_id = ?1 and course_id = ?2",nativeQuery = true)
	public Result1 getResult(int userId,int courseId);
}

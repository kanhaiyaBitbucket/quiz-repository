package com.quiz.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.quiz.entity.CourseType;
import com.quiz.model.CourseDetail;

public interface CourseTypeRepository extends JpaRepository<CourseType, Integer> {

	@Query("select new com.quiz.model.CourseDetail(c.id,c.name) from CourseType c")
	public List<CourseDetail> getCourseDetails();
}

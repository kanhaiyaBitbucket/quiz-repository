package com.quiz.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.quiz.entity.QuestionDetail;

public interface QuestionDetailRepository extends JpaRepository<QuestionDetail,  Integer> {

	@Query(value = "select * from question_table where course_id = ?1 ", nativeQuery = true)
	public List<QuestionDetail> findByCourseId(int id);
}

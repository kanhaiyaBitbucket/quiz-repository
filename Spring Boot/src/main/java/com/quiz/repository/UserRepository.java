package com.quiz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.quiz.entity.RegistredUser;
import com.quiz.model.User;

public interface UserRepository extends JpaRepository<RegistredUser, Integer> {

	@Query("select new com.quiz.model.User(r.id,r.email,r.password) from RegistredUser r where email = ?1 ")
	public User getUser(String email);
}

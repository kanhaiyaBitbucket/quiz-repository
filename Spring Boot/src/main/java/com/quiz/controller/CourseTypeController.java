package com.quiz.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.quiz.entity.CourseType;
import com.quiz.model.CourseDetail;
import com.quiz.service.CourseTypeService;

@RestController
@CrossOrigin(origins = "*")
public class CourseTypeController {

	@Autowired
	private CourseTypeService courseTypeService;

	@GetMapping("/course/{id}")
	public Optional<CourseType> getCourseById(@PathVariable("id") int id) {
		return courseTypeService.getCourseTypeById(id);
	}

	@GetMapping("/courses")
	public List<CourseDetail> getCourses() {
		return courseTypeService.getCourses();
	}
}

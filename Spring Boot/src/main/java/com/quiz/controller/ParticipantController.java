package com.quiz.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.quiz.entity.Participant;
import com.quiz.model.ParticipantModel;
import com.quiz.model.Result1;
import com.quiz.service.ParticipantService;

@RestController
@CrossOrigin(origins = "*")
public class ParticipantController {

	@Autowired
	private ParticipantService participantService;
	
	@GetMapping("/participant/{id}")
	public Optional<Participant> getParticipantById(@PathVariable("id") int id) {
		return participantService.getParticipentById(id);
	}
	
	@PostMapping("/participant")
	public void saveParticipant(@RequestBody ParticipantModel participant) {
		 participantService.saveParticipent(participant);
	}
	
	@GetMapping("/participant/{uid}/{cid}")
	public Participant getParticipant(@PathVariable("uid") int userId,@PathVariable("cid") int courseId) {
		return participantService.getParticipent(userId, courseId);
	}
	
	@GetMapping("/result/{uid}/{cid}")
	public Result1 getResult(@PathVariable("uid") int userId,@PathVariable("cid") int courseId) {
		return participantService.getResult(userId, courseId);
	}
}

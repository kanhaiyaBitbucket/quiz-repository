package com.quiz.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.quiz.entity.RegistredUser;
import com.quiz.model.User;
import com.quiz.service.RegisterdUserService;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
	private RegisterdUserService registerdUserService;

	@GetMapping("/userdetail/{id}")
	public Optional<RegistredUser> getUserById(@PathVariable("id") int id) {
		return registerdUserService.getUserById(id);
	}

	@PostMapping("/user")
	public void saveUser(@RequestBody RegistredUser registredUser) {
		registerdUserService.saveUserDetail(registredUser);
	}

	@GetMapping("/user/{email}")
	public User getUser(@PathVariable("email") String email) {
		return registerdUserService.getUser(email);
	}
}

package com.quiz.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.quiz.entity.QuestionDetail;
import com.quiz.service.QuestionDetailService;

@RestController
@CrossOrigin(origins = "*")
public class QuestionController {

	@Autowired
	private QuestionDetailService questionDetailService;
	
	@GetMapping("/question/{id}")
	public Optional<QuestionDetail> getQuestionById(@PathVariable("id") int id) {
		return questionDetailService.getQuestionById(id);
	}
	
	@GetMapping("/questions/{id}")
	public List<QuestionDetail> getQuestionByCourseId(@PathVariable("id") int id) {
		return questionDetailService.getQuestionByCourseId(id);
	}
}

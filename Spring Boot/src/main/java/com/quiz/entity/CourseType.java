package com.quiz.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "course_type")
public class CourseType {

	@Id
	private int id;
	private String name;
	@OneToMany(targetEntity = Participant.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "course_id", referencedColumnName = "id")
	private List<Participant> participants;
	@OneToMany(targetEntity = QuestionDetail.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "course_id", referencedColumnName = "id")
	private List<QuestionDetail> questionDetails;

	public List<QuestionDetail> getQuestionDetails() {
		return questionDetails;
	}

	public void setQuestionDetails(List<QuestionDetail> questionDetails) {
		this.questionDetails = questionDetails;
	}

	public List<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

package com.quiz.model;

public interface Result1 {

    String getName();
    int getScore();
    int getTimespent();
}

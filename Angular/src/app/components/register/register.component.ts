import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  genders:string[] = ["male","female","other"]
  countrys:string[] = ["uzbekistan","russia","united State","india","afganistan"]
  isRegisterd:boolean=false;

  user:any = {
    "firstName" : "",
    "lastName" : "",
    "email" : "",
    "gender" : "male",
    "city" : "",
    "country" : "india",
    "password" : "",
    "cnfPassword" : ""
  }

  
  registerUser:any={
     "name": "",
     "email": "",
     "gender": "",
     "city": "",
     "country": "",
     "password": ""
  }
 
  register(){
    this.registerUser.name = this.user.firstName + this.user.lastName;
    this.registerUser.email = this.user.email;
    this.registerUser.gender = this.user.gender;
    this.registerUser.city = this.user.city;
    this.registerUser.country = this.user.country;
    this.registerUser.password = this.user.password;
    this.http.post("http://localhost:8081/user",this.registerUser).subscribe((data)=>{
         this.isRegisterd = true;
    })
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuizService } from 'src/app/services/quiz.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {



  constructor(private routes: ActivatedRoute, private quizserv: QuizService,
     private router:Router, private http: HttpClient) {}

  courseId:number;
  Questions:any[];
  increment:number=0;
  dicrement:number=0;
  question:any;
  answer:number;
  correct:number=0;
  isSubmited:boolean=false;
  userId:number;
  path:string = "http://localhost:8081/participant"

  participant:any={
     "score":0,
     "timespent":0,
     "courseId":0,
     "userId":0
  }

  

  ngOnInit() {
    this.routes.paramMap.subscribe((parm)=>{
        this.courseId = Number(parm.get('cid'));
        this.userId = Number(parm.get('uid'));
        this.quizserv.getQuestion(this.courseId).subscribe((data)=>{
          this.Questions = data;
          this.question = this.Questions[this.increment];
        });
    });
    
  }

  next(){

      if(this.answer != undefined){
         if(this.answer == this.question.answer){
           this.correct++;
         }
      }

      if(this.increment<this.Questions.length-1){
        this.increment++;
        this.question = this.Questions[this.increment];
      }

      this.answer = undefined;
  }

  submit(time){
    this.isSubmited = true;
    this.participant.score = this.correct;
    this.participant.courseId = this.courseId;
    this.participant.userId = this.userId;
    this.participant.timespent = time;
    
    if(time != undefined){
      this.http.post(this.path,this.participant).subscribe((data)=>{
        this.router.navigate([this.userId,"result",this.courseId,this.userId]);
      });
    }

  }


}

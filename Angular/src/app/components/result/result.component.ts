import { Component, OnInit } from '@angular/core';
import { ResultService } from 'src/app/services/result.service';
import { Result } from 'src/app/common/result';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  constructor(private resultServ: ResultService, private routes: ActivatedRoute, private router:Router) { }

  result:Result = new Result();
  userId:number;
  courseId:number;

  ngOnInit() {

      this.routes.paramMap.subscribe((parm)=>{
      this.courseId = Number(parm.get('cid'));
      this.userId = Number(parm.get('uid'));
      this.resultServ.getResult(this.userId,this.courseId).subscribe((data)=>{
        this.result = data;
     });
  });

  }

  OnSubmit(){
    this.router.navigate([this.userId]);
  }

}

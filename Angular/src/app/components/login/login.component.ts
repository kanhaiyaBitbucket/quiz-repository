import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/common/user';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginserv: LoginService, private router:Router) { }

  ngOnInit() {
  }

  userAuth:User = new User();
  invalid:boolean = false;

  user = {
    "userName" : "",
    "password" : ""
  }

  call(){
    this.loginserv.getUser(this.user.userName).subscribe((data)=>{
      this.userAuth = data;
  });
  }

  login(){
    if(this.userAuth == null || this.userAuth.password == undefined){
      this.invalid = true;
    }else{
      
        if(this.userAuth.password == this.user.password){
          this.router.navigate([this.userAuth.id]);
        }else{
          this.invalid = true;
        }
     
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { CourseService } from 'src/app/services/course.service';
import { CourseDetail } from 'src/app/common/coursedetail';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';
import { UserDetail } from 'src/app/common/userdetails';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private courseserv: CourseService, private routes:ActivatedRoute,
     private regiserv: RegisterService, private router:Router) { }

  courseDetail:any[];
  userId:number;
  user:UserDetail = new UserDetail();
  path:string="/" + this.userId +"/quiz/" + 0 + "/" + this.userId;
  isDone:boolean=false;
  notDone:boolean=false;

  ngOnInit() {
    this.courseserv.getCources().subscribe((data)=>{
      this.courseDetail = data;
    });

    this.routes.paramMap.subscribe((parm)=>{
      this.userId = Number(parm.get('uid'));
    });
  }

  check(courseId){
    
    this.regiserv.getParticipant(this.userId,courseId).subscribe((data)=>{
      this.user = data;
      if(this.user == undefined){
          //  this.path = "/" + this.userId +"/quiz/" + courseId + "/" + this.userId;
           this.router.navigate([this.userId,"quiz",courseId,this.userId]);
           this.isDone = false;
           this.notDone = true;
           
      }else{
          //  this.path = "/" + this.userId +"/quiz/" + 0 + "/" + this.userId;
           this.router.navigate([this.userId]);
           this.notDone = true;
           this.isDone = true;
           
      }
    })

  }


}

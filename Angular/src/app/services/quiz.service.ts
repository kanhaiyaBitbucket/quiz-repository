import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { QuestionInfo } from '../common/questiondetal';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(private http:HttpClient) { }


  getQuestion(courseId:number):Observable<QuestionInfo[]> {
    return this.http.get<QuestionInfo[]>("http://localhost:8081/questions/" + courseId);
  }
}

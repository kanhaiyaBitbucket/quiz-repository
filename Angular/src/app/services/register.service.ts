import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDetail } from '../common/userdetails';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  getParticipant(userId:number,courseId:number):Observable<UserDetail>{
    return this.http.get<UserDetail>("http://localhost:8081/participant/" + userId + "/" + courseId);
  }
}

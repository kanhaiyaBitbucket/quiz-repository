import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Result } from '../common/result';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  constructor(private http:HttpClient) { }

  getResult(userId:number,courseId:number):Observable<Result>{
    return this.http.get<Result>("http://localhost:8081/result/" + userId + "/" + courseId);
  }
}

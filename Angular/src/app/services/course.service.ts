import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CourseDetail } from '../common/coursedetail';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http:HttpClient) { }

  getCources():Observable<CourseDetail[]>{
    return this.http.get<CourseDetail[]>("http://localhost:8081/courses");
  }
}
